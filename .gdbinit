set disassembly-flavor intel
set print asm-demangle on
set unwind-on-terminating-exception off
set print object on
set prompt [ ınȷɔɔnɔɥənɼɼɼ } 🌸(> ^ _ ^)> 
set extended-prompt \n\[\e[0;38;5;213;1m\]GDB \v \[\e[0;38;5;117;1m\]\w \[\e[0;38;5;215;1m\]\f\n\[\e[0;38;5;193m\][ ınȷɔɔnɔɥənɼɼɼ } \[\e[0;38;5;225m\]🌸(> ^ _ ^)> \[\e[0m\]

python
# Change this to the real path to the pp directory or this won't work.
sys.path.insert(0, "/home/uruwi/kozet-gdb-stuff/pp")
import glm_pp
import kfp_pp
end


