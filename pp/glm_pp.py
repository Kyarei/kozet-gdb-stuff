import gdb.printing
import pputils
import itertools

_type_letters = {
    gdb.TYPE_CODE_FLT: "d", # or "f"
    gdb.TYPE_CODE_INT: "i",
    gdb.TYPE_CODE_BOOL: "b"
}

def _get_type_letter(code, size):
    if code == gdb.TYPE_CODE_FLT:
        if size == 4: return ""
        if size == 8: return "d"
        return "t"
    return _type_letters.get(code, "t")

def _vec_info(v):
    # vec contains either a union of structs or a struct of unions, depending on
    # configuration. gdb can't properly access the named members, and in some
    # cases the names are wrong.
    # It would be simple to cast to an array, similarly to how operator[] is
    # implemented, but values returned by functions called from gdb don't have
    # an address.
    # Instead, recursively find all fields of required type and sort by offset.
    T = v.type.template_argument(1)
    letter = _get_type_letter(T.code, T.sizeof)
    inner = T.name if letter == "t" else None
    length = v.type.sizeof // T.sizeof
    items = {}
    def find(v, bitpos):
        t = v.type.strip_typedefs()
        if t == T:
            items[bitpos] = v
        elif t.code in (gdb.TYPE_CODE_STRUCT, gdb.TYPE_CODE_UNION):
            for f in t.fields():
                if hasattr(f, "bitpos"): # not static
                    find(v[f], bitpos + f.bitpos)
    find(v, 0)
    assert len(items) == length, "{} != {}".format(len(items), length)
    items = [str(f) for k, f in sorted(items.items())]
    return letter, length, items, inner

class VecPrinter:
    def __init__(self, v):
        self.v = v

    def to_string(self):
        letter, length, items, inner = _vec_info(self.v)
        tinner = "<{}>".format(inner) if inner else ""
        return "{}vec{}{}({})".format(letter, length, tinner, ", ".join(items))
    
    """def children(self):
        letter, length, items, inner = _vec_info(self.v)
        zl = itertools.zip_longest('xyzw', items)
        return itertools.takewhile(lambda p: p[1] is not None, zl)
    
    def display_hint(self):
        return "array"
        """

class MatPrinter:
    def __init__(self, v):
        self.v = v

    def to_string(self):
        V = self.v["value"]
        columns = []
        inner = None
        for i in range(V.type.range()[1] + 1):
            letter, length, items, innert = _vec_info(V[i])
            inner = innert
            columns.append("({})".format(", ".join(items)))
        tinner = "<{}>".format(inner) if inner else ""
        return "{}mat{}x{}{}({})".format(
            letter, len(columns), length, tinner, ", ".join(columns))

def build_pretty_printer():
    p = pputils.TypenamePrettyPrinter("glm_pp")
    p.add_printer("glm::vec", "glm::vec", VecPrinter);
    p.add_printer("glm::mat", "glm::mat", MatPrinter);
    return p

gdb.printing.register_pretty_printer(gdb.current_objfile(),
                                     build_pretty_printer())
