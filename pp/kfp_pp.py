import gdb.printing
import pputils
import math

def postfix(underlying_type, fractional_bits):
    type_bits = underlying_type.sizeof * 8
    is_signed = not gdb.types.get_basic_type(
    	underlying_type).name.startswith("unsigned")
    integral_bits = type_bits - fractional_bits
    if integral_bits == 0:
        if is_signed: return "sfrac{}".format(fractional_bits)
        return "frac{}".format(fractional_bits)
    if is_signed: return "s{}_{}".format(integral_bits, fractional_bits)
    return "u{}_{}".format(integral_bits, fractional_bits)

class FixedPrinter:
    def __init__(self, v):
        self.v = v

    def to_string(self):
    	typeinfo = gdb.types.get_basic_type(self.v.type)
    	underlying_type = typeinfo.template_argument(0)
    	fractional_bits = int(str(typeinfo.template_argument(1)))
    	underlying_value = float(str(self.v["underlying"]))
    	as_floating = math.ldexp(underlying_value, -fractional_bits)
    	pf = postfix(underlying_type, fractional_bits)
    	return "{}_{}".format(as_floating, pf)

def build_pretty_printer():
    p = pputils.TypenamePrettyPrinter("kfp_pp")
    p.add_printer("kfp::Fixed", "kfp::Fixed", FixedPrinter);
    return p

gdb.printing.register_pretty_printer(gdb.current_objfile(),
                                     build_pretty_printer())

