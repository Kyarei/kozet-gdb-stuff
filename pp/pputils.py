import gdb.printing
import re

_before_angle = re.compile(r"[A-Za-z0-9_:]+")

class TypenamePrettyPrinter(gdb.printing.PrettyPrinter):
    class TypenameSubprinter(gdb.printing.SubPrettyPrinter):
        def __init__(self, name, tname, gen_printer):
            super(TypenamePrettyPrinter.TypenameSubprinter, self).__init__(name)
            self.tname = tname
            self.gen_printer = gen_printer
    def __init__(self, name):
        super(TypenamePrettyPrinter, self).__init__(name, [])
        self.lookup = {}
    def add_printer(self, name, tname, gen_printer):
        sp = self.TypenameSubprinter(name, tname, gen_printer)
        self.lookup[tname] = sp
        self.subprinters.append(sp)
    def __call__(self, val):
        typeobj = gdb.types.get_basic_type(val.type)
        typename = typeobj.tag
        if typename == None: return None
        match = re.match(_before_angle, typename).group(0)
        if match not in self.lookup: return None
        return self.lookup[match].gen_printer(val)

